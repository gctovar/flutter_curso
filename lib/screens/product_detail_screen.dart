import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter_launch/flutter_launch.dart';
import 'package:url_launcher/url_launcher.dart';

import '../providers/products.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProductDetailScreen extends StatelessWidget {
  static const routeName = 'Product-detail';

  @override
  Widget build(BuildContext context) {
    final productId = ModalRoute.of(context).settings.arguments as String;
    final loadedProduct =
        Provider.of<Products>(context, listen: false).findById(productId);

    Widget imageCarosel = Container(
      height: 300,
      width: double.infinity,
      child: Carousel(
        boxFit: BoxFit.cover,
        images: [
          Image.network(loadedProduct.imageUrl01),
          Image.network(loadedProduct.imageUrl02),
          Image.network(loadedProduct.imageUrl03),
        ],
        autoplay: false,
        animationCurve: Curves.fastOutSlowIn,
        dotSize: 4.0,
        dotColor: Colors.red,
        indicatorBgPadding: 4.0,
      ),
    );

    void whatsAppOpen() async {
      await FlutterLaunch.launchWathsApp(
          phone: loadedProduct.whatsapp.toString(),
          message: "Que tal, me interasa el automovil ${loadedProduct.title}");
    }

    void _lauchCaller(String number) async {
      var url = "tel: ${number.toString()}";
      if (await canLaunch(url)) {
        await launch(url);
      } else {
        throw "Nose pudo realizar llamada";
      }
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(loadedProduct.title),
      ),
      body: ListView(
        children: <Widget>[
          imageCarosel,
          SizedBox(
            height: 10,
          ),
          Container(
              color: Colors.green,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(Icons.attach_money),
                  Text(
                    loadedProduct.price,
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.black, fontSize: 20),
                  ),
                ],
              )),
          SizedBox(
            height: 10,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10),
            width: double.infinity,
            child: Text(
              'Descripción',
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 15,
                  fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Container(
            color: Colors.green,
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            width: double.infinity,
            child: Text(
              loadedProduct.description,
              textAlign: TextAlign.left,
              softWrap: true,
            ),
          ),
          Container(
            color: Colors.yellow,
            child: Row(
              children: <Widget>[
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 5, vertical: 15),
                ),
                Icon(Icons.airport_shuttle),
                Text(' Kilometros: '),
                Text(loadedProduct.km.toString()),
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10),
            width: double.infinity,
            child: Text(
              'Contacto',
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 15,
                  fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            child: Row(
              children: <Widget>[
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 5, vertical: 25),
                ),
                IconButton(
                  icon: Icon(Icons.call),
                  color: Colors.green,
                  onPressed: () {
                    _lauchCaller(loadedProduct.phone);
                  },
                ),
                Text('Telefono'),
                Text(loadedProduct.phone),
                Expanded(
                    child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: GestureDetector(
                    onTap: () {
                      whatsAppOpen();
                    },
                    child: Container(
                      height: 40,
                      decoration: BoxDecoration(
                        color: Colors.green[800],
                        borderRadius: BorderRadius.circular(10),
                      ),
                      padding: const EdgeInsets.symmetric(
                        vertical: 10,
                      ),
                      child: Text(
                        'WhatApp',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 15,
                            color: Colors.white,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ))
              ],
            ),
          )
        ],
      ),
    );
  }
}
