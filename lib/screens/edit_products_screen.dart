import 'package:cursoflutter/providers/products.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../providers/product.dart';

class EditProductsScreen extends StatefulWidget {
  static const routeName = 'Edit-products-screen';

  @override
  _EditProductsScreenState createState() => _EditProductsScreenState();
}

class _EditProductsScreenState extends State<EditProductsScreen> {
  final _priceFocusNode = FocusNode();
  final _drescriptionFocusNode = FocusNode();
  final _kmFocusNode = FocusNode();
  final _phoneFocusNode = FocusNode();
  final _whatsAppFocusNode = FocusNode();
  final _imageUrl01Controller = TextEditingController();
  final _imageUrl02Controller = TextEditingController();
  final _imageUrl03Controller = TextEditingController();

  final _imageUrl01FocusNote = FocusNode();
  final _imageUrl02FocusNote = FocusNode();
  final _imageUrl03FocusNote = FocusNode();

  final _form = GlobalKey<FormState>();

  var _editedProduct = Product(
      id: null,
      title: '',
      description: '',
      km: 0,
      price: '',
      imageUrl01: '',
      imageUrl02: '',
      imageUrl03: '',
      phone: '',
      whatsapp: 0);

  var _initValues = {
    'title': '',
    'price': '',
    'description': '',
    'km': '',
    'phone': '',
    'whatsapp': '',
    'imageUrl01': '',
    'imageUrl02': '',
    'imageUrl03': '',
  };

  var _isInit = true;
  var _isLoading = false;

  @override
  void initState() {
    _imageUrl01FocusNote.addListener(_updateImageUrl01);
    _imageUrl02FocusNote.addListener(_updateImageUrl02);
    _imageUrl03FocusNote.addListener(_updateImageUrl03);
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (_isInit) {
      final productId = ModalRoute.of(context).settings.arguments as String;
      if (productId != null) {
        _editedProduct =
            Provider.of<Products>(context, listen: false).findById(productId);
        _initValues = {
          'title': _editedProduct.title,
          'price': _editedProduct.price,
          'description': _editedProduct.description,
          'km': _editedProduct.km.toString(),
          'phone': _editedProduct.phone,
          'whatsapp': _editedProduct.whatsapp.toString(),
          'imageUrl01': _editedProduct.imageUrl01,
          'imageUrl02': _editedProduct.imageUrl02,
          'imageUrl03': _editedProduct.imageUrl03,
        };
        _imageUrl01Controller.text = _editedProduct.imageUrl01;
        _imageUrl02Controller.text = _editedProduct.imageUrl02;
        _imageUrl03Controller.text = _editedProduct.imageUrl03;
      }
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _priceFocusNode.dispose();
    _drescriptionFocusNode.dispose();
    _kmFocusNode.dispose();
    _phoneFocusNode.dispose();
    _whatsAppFocusNode.dispose();
    _imageUrl01Controller.dispose();
    _imageUrl02Controller.dispose();
    _imageUrl03Controller.dispose();
    _imageUrl01FocusNote.dispose();
    _imageUrl02FocusNote.dispose();
    _imageUrl03FocusNote.dispose();
    _imageUrl01FocusNote.removeListener(_updateImageUrl01);
    _imageUrl02FocusNote.removeListener(_updateImageUrl02);
    _imageUrl03FocusNote.removeListener(_updateImageUrl03);

    super.dispose();
  }

  void _updateImageUrl01() {
    if (!_imageUrl01FocusNote.hasFocus) {
      setState(() {});
    }
  }

  void _updateImageUrl02() {
    if (!_imageUrl02FocusNote.hasFocus) {
      setState(() {});
    }
  }

  void _updateImageUrl03() {
    if (!_imageUrl03FocusNote.hasFocus) {
      setState(() {});
    }
  }

  Future<void> _safeform() async {
    _form.currentState.save();
    setState(() {
      _isLoading = true;
    });

    if (_editedProduct.id != null) {
      await Provider.of<Products>(context, listen: false)
          .updateProduct(_editedProduct.id, _editedProduct);
    } else {
      try {
        await Provider.of<Products>(context, listen: false)
            .addProduct(_editedProduct);
      } catch (error) {
        await showDialog(
            context: context,
            builder: (ctx) => AlertDialog(
                  title: Text('Ocurrio  un error'),
                  content: Text('Algo Malo Ocurrio '),
                  actions: <Widget>[
                    FlatButton(
                      child: Text('Ok'),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    )
                  ],
                ));
      }
    }
    setState(() {
      _isLoading = false;
    });
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Edit product'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.save),
            onPressed: _safeform,
          )
        ],
      ),
      body: _isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Padding(
              padding: const EdgeInsets.all(15),
              child: Form(
                  key: _form,
                  child: ListView(
                    children: <Widget>[
                      TextFormField(
                        initialValue: _initValues['title'],
                        decoration: InputDecoration(
                            labelText: 'Title',
                            labelStyle: TextStyle(color: Colors.black)),
                        textInputAction: TextInputAction.next,
                        onFieldSubmitted: (_) {
                          FocusScope.of(context).requestFocus(_priceFocusNode);
                        },
                        onSaved: (value) {
                          _editedProduct = Product(
                              id: _editedProduct.id,
                              title: value,
                              description: _editedProduct.description,
                              km: _editedProduct.km,
                              price: _editedProduct.price,
                              imageUrl01: _editedProduct.imageUrl01,
                              imageUrl02: _editedProduct.imageUrl02,
                              imageUrl03: _editedProduct.imageUrl03,
                              phone: _editedProduct.phone,
                              whatsapp: _editedProduct.whatsapp,
                              isFavorite: _editedProduct.isFavorite);
                        },
                        style: TextStyle(color: Colors.black),
                      ),
                      TextFormField(
                        initialValue: _initValues['price'],
                        decoration: InputDecoration(
                            labelText: 'Price',
                            labelStyle: TextStyle(color: Colors.black)),
                        textInputAction: TextInputAction.next,
                        style: TextStyle(color: Colors.black),
                        keyboardType: TextInputType.number,
                        focusNode: _priceFocusNode,
                        onFieldSubmitted: (_) {
                          FocusScope.of(context)
                              .requestFocus(_drescriptionFocusNode);
                        },
                        onSaved: (value) {
                          _editedProduct = Product(
                              id: _editedProduct.id,
                              title: _editedProduct.title,
                              description: _editedProduct.description,
                              km: _editedProduct.km,
                              price: value,
                              imageUrl01: _editedProduct.imageUrl01,
                              imageUrl02: _editedProduct.imageUrl02,
                              imageUrl03: _editedProduct.imageUrl03,
                              phone: _editedProduct.phone,
                              whatsapp: _editedProduct.whatsapp,
                              isFavorite: _editedProduct.isFavorite);
                        },
                      ),
                      TextFormField(
                        initialValue: _initValues['description'],
                        decoration: InputDecoration(
                            labelText: 'Description',
                            labelStyle: TextStyle(color: Colors.black)),
                        maxLines: 3,
                        style: TextStyle(color: Colors.black),
                        keyboardType: TextInputType.multiline,
                        focusNode: _drescriptionFocusNode,
                        onFieldSubmitted: (_) {
                          FocusScope.of(context).requestFocus(_kmFocusNode);
                        },
                        onSaved: (value) {
                          _editedProduct = Product(
                              id: _editedProduct.id,
                              title: _editedProduct.title,
                              description: value,
                              km: _editedProduct.km,
                              price: _editedProduct.price,
                              imageUrl01: _editedProduct.imageUrl01,
                              imageUrl02: _editedProduct.imageUrl02,
                              imageUrl03: _editedProduct.imageUrl03,
                              phone: _editedProduct.phone,
                              whatsapp: _editedProduct.whatsapp,
                              isFavorite: _editedProduct.isFavorite);
                        },
                      ),
                      TextFormField(
                        initialValue: _initValues['km'],
                        decoration: InputDecoration(
                            labelText: 'KM',
                            labelStyle: TextStyle(color: Colors.black)),
                        textInputAction: TextInputAction.next,
                        style: TextStyle(color: Colors.black),
                        keyboardType: TextInputType.number,
                        focusNode: _kmFocusNode,
                        onFieldSubmitted: (_) {
                          FocusScope.of(context).requestFocus(_phoneFocusNode);
                        },
                        onSaved: (value) {
                          _editedProduct = Product(
                              id: _editedProduct.id,
                              title: _editedProduct.title,
                              description: _editedProduct.description,
                              km: int.parse(value),
                              price: _editedProduct.price,
                              imageUrl01: _editedProduct.imageUrl01,
                              imageUrl02: _editedProduct.imageUrl02,
                              imageUrl03: _editedProduct.imageUrl03,
                              phone: _editedProduct.phone,
                              whatsapp: _editedProduct.whatsapp,
                              isFavorite: _editedProduct.isFavorite);
                        },
                      ),
                      TextFormField(
                        initialValue: _initValues['phone'],
                        decoration: InputDecoration(
                            labelText: 'Phone',
                            labelStyle: TextStyle(color: Colors.black)),
                        textInputAction: TextInputAction.next,
                        style: TextStyle(color: Colors.black),
                        keyboardType: TextInputType.number,
                        focusNode: _phoneFocusNode,
                        onFieldSubmitted: (_) {
                          FocusScope.of(context)
                              .requestFocus(_whatsAppFocusNode);
                        },
                        onSaved: (value) {
                          _editedProduct = Product(
                              id: _editedProduct.id,
                              title: _editedProduct.title,
                              description: _editedProduct.description,
                              km: _editedProduct.km,
                              price: _editedProduct.price,
                              imageUrl01: _editedProduct.imageUrl01,
                              imageUrl02: _editedProduct.imageUrl02,
                              imageUrl03: _editedProduct.imageUrl03,
                              phone: value,
                              whatsapp: _editedProduct.whatsapp,
                              isFavorite: _editedProduct.isFavorite);
                        },
                      ),
                      TextFormField(
                        initialValue: _initValues['whatsapp'],
                        decoration: InputDecoration(
                            labelText: 'WhatsApp',
                            labelStyle: TextStyle(color: Colors.black)),
                        textInputAction: TextInputAction.next,
                        style: TextStyle(color: Colors.black),
                        keyboardType: TextInputType.number,
                        focusNode: _whatsAppFocusNode,
                        onFieldSubmitted: (_) {
                          FocusScope.of(context)
                              .requestFocus(_imageUrl01FocusNote);
                        },
                        onSaved: (value) {
                          _editedProduct = Product(
                              id: _editedProduct.id,
                              title: _editedProduct.title,
                              description: _editedProduct.description,
                              km: _editedProduct.km,
                              price: _editedProduct.price,
                              imageUrl01: _editedProduct.imageUrl01,
                              imageUrl02: _editedProduct.imageUrl02,
                              imageUrl03: _editedProduct.imageUrl03,
                              phone: _editedProduct.phone,
                              whatsapp: int.parse(value),
                              isFavorite: _editedProduct.isFavorite);
                        },
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          Container(
                            width: 100,
                            height: 100,
                            margin: EdgeInsets.only(top: 8, right: 10),
                            decoration: BoxDecoration(
                                border: Border.all(
                              width: 1,
                              color: Colors.orange,
                            )),
                            child: _imageUrl01Controller.text.isEmpty
                                ? Text('Ingrese uba url')
                                : FittedBox(
                                    child: Image.network(
                                      _imageUrl01Controller.text,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                          ),
                          Expanded(
                              child: TextFormField(
                            decoration:
                                InputDecoration(labelText: 'Image01 url'),
                            keyboardType: TextInputType.url,
                            textInputAction: TextInputAction.done,
                            controller: _imageUrl01Controller,
                            focusNode: _imageUrl01FocusNote,
                            onFieldSubmitted: (_) {
                              FocusScope.of(context)
                                  .requestFocus(_imageUrl02FocusNote);
                            },
                            onSaved: (value) {
                              _editedProduct = Product(
                                  id: _editedProduct.id,
                                  title: _editedProduct.title,
                                  description: _editedProduct.description,
                                  km: _editedProduct.km,
                                  price: _editedProduct.price,
                                  imageUrl01: value,
                                  imageUrl02: _editedProduct.imageUrl02,
                                  imageUrl03: _editedProduct.imageUrl03,
                                  phone: _editedProduct.phone,
                                  whatsapp: _editedProduct.whatsapp,
                                  isFavorite: _editedProduct.isFavorite);
                            },
                          ))
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          Container(
                            width: 100,
                            height: 100,
                            margin: EdgeInsets.only(top: 8, right: 10),
                            decoration: BoxDecoration(
                                border: Border.all(
                              width: 1,
                              color: Colors.orange,
                            )),
                            child: _imageUrl02Controller.text.isEmpty
                                ? Text('Ingrese uba url')
                                : FittedBox(
                                    child: Image.network(
                                      _imageUrl02Controller.text,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                          ),
                          Expanded(
                              child: TextFormField(
                            decoration:
                                InputDecoration(labelText: 'Image02 url'),
                            keyboardType: TextInputType.url,
                            textInputAction: TextInputAction.done,
                            controller: _imageUrl02Controller,
                            focusNode: _imageUrl02FocusNote,
                            onFieldSubmitted: (_) {
                              FocusScope.of(context)
                                  .requestFocus(_imageUrl03FocusNote);
                            },
                            onSaved: (value) {
                              _editedProduct = Product(
                                  id: _editedProduct.id,
                                  title: _editedProduct.title,
                                  description: _editedProduct.description,
                                  km: _editedProduct.km,
                                  price: _editedProduct.price,
                                  imageUrl01: _editedProduct.imageUrl01,
                                  imageUrl02: value,
                                  imageUrl03: _editedProduct.imageUrl03,
                                  phone: _editedProduct.phone,
                                  whatsapp: _editedProduct.whatsapp,
                                  isFavorite: _editedProduct.isFavorite);
                            },
                          ))
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          Container(
                            width: 100,
                            height: 100,
                            margin: EdgeInsets.only(top: 8, right: 10),
                            decoration: BoxDecoration(
                                border: Border.all(
                              width: 1,
                              color: Colors.orange,
                            )),
                            child: _imageUrl03Controller.text.isEmpty
                                ? Text('Ingrese uba url')
                                : FittedBox(
                                    child: Image.network(
                                      _imageUrl03Controller.text,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                          ),
                          Expanded(
                              child: TextFormField(
                            decoration:
                                InputDecoration(labelText: 'Image03 url'),
                            keyboardType: TextInputType.url,
                            textInputAction: TextInputAction.done,
                            controller: _imageUrl03Controller,
                            focusNode: _imageUrl03FocusNote,
                            onFieldSubmitted: (_) {
                              _safeform();
                            },
                            onSaved: (value) {
                              _editedProduct = Product(
                                  id: _editedProduct.id,
                                  title: _editedProduct.title,
                                  description: _editedProduct.description,
                                  km: _editedProduct.km,
                                  price: _editedProduct.price,
                                  imageUrl01: _editedProduct.imageUrl01,
                                  imageUrl02: _editedProduct.imageUrl02,
                                  imageUrl03: value,
                                  phone: _editedProduct.phone,
                                  whatsapp: _editedProduct.whatsapp,
                                  isFavorite: _editedProduct.isFavorite);
                            },
                          ))
                        ],
                      )
                    ],
                  )),
            ),
    );
  }
}
