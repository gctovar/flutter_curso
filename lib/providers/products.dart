import 'product.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import '../models/http_exception.dart';

class Products with ChangeNotifier {
  List<Product> _items = [
    Product(
        id: 'a1',
        title: 'Sentra 2005',
        description: 'Muy bueno',
        km: 160000,
        price: '98,000',
        imageUrl01:
            'https://static4.abc.es/media/motor/2019/10/15/coche-negro-kgwB--620x349@abc.jpg',
        imageUrl02:
            'https://s1.eestatic.com/2019/07/30/omicrono/Coches-Xiaomi-Inteligencia_artificial-Omicrono_417719200_131255309_1024x576.jpg',
        imageUrl03:
            'https://images1.autocasion.com/unsafe/900x600/actualidad/wp-content/uploads/2016/07/q7_aper.jpg',
        phone: '+52 442 6755 229',
        whatsapp: 524426755229),
    Product(
        id: 'a2',
        title: 'Ferrari 2011',
        description: 'Cool',
        km: 270000,
        price: '120,000',
        imageUrl01:
            'https://www.autobild.es/sites/autobild.es/public/styles/main_element/public/dc/fotos/Ferrari-458_Italia_2011_01.jpg?itok=1oxlfvv7',
        imageUrl02:
            'https://soymotor.com/sites/default/files/styles/mega/public/imagenes/noticia/ferrari-70th-anniversary-at-goodwood_-_soymotor.jpg?itok=hlac0bwN',
        imageUrl03:
            'https://d1eip2zddc2vdv.cloudfront.net/dphotos/750x400/17874-1528190287.jpg',
        phone: '+52 442 6755 229',
        whatsapp: 524426755229),
  ];

  final String authToken;
  final String userId;

  Products(this.authToken, this.userId, this._items);

  List<Product> get items {
    return [..._items];
  }

  List<Product> get favoriteItems {
    return _items.where((productItem) => productItem.isFavorite).toList();
  }

  Product findById(String id) {
    return _items.firstWhere((prod) => prod.id == id);
  }

  Future<void> fetchAndSetProducts() async {
    var url =
        'https://ventaautos-fac9c.firebaseio.com/products.json?auth=$authToken';
    try {
      final response = await http.get(url);
      final extractedData = json.decode(response.body) as Map<String, dynamic>;
      if (extractedData == null) {
        return;
      }
      url =
          'https://ventaautos-fac9c.firebaseio.com/userFavorites/$userId/.json?auth=$authToken';
      final favoriteResponse = await http.get(url);
      final favoriteData = json.decode(favoriteResponse.body);
      final List<Product> loaderProducts = [];

      extractedData.forEach((prodId, prodData) {
        loaderProducts.insert(
            (0),
            Product(
              id: prodId,
              title: prodData['title'],
              description: prodData['description'],
              km: prodData['km'],
              price: prodData['price'],
              imageUrl01: prodData['imageUrl01'],
              imageUrl02: prodData['imageUrl02'],
              imageUrl03: prodData['imageUrl03'],
              phone: prodData['phone'],
              whatsapp: prodData['whatsapp'],
              isFavorite:
                  favoriteData == null ? false : favoriteData[prodId] ?? false,
            ));
      });
      _items = loaderProducts;
      notifyListeners();
    } catch (error) {
      throw (error);
    }
  }

  Future<void> addProduct(Product product) async {
    final url =
        'https://ventaautos-fac9c.firebaseio.com/products.json?auth=$authToken';
    try {
      final response = await http.post(url,
          body: jsonEncode({
            'title': product.title,
            'price': product.price,
            'description': product.description,
            'km': product.km,
            'phone': product.phone,
            'whatsapp': product.whatsapp,
            'imageUrl01': product.imageUrl01,
            'imageUrl02': product.imageUrl02,
            'imageUrl03': product.imageUrl03,
          }));
      final newProduct = Product(
          title: product.title,
          price: product.price,
          description: product.description,
          km: product.km,
          phone: product.phone,
          whatsapp: product.whatsapp,
          imageUrl01: product.imageUrl01,
          imageUrl02: product.imageUrl02,
          imageUrl03: product.imageUrl03,
          isFavorite: product.isFavorite,
          id: json.decode(response.body)['name']);
      _items.add(newProduct);
      // _items.insert(0, newProducto);
      notifyListeners();
    } catch (error) {
      print(error);
      throw error;
    }
  }

  Future<void> updateProduct(String id, Product newProduct) async {
    final prodIndex = _items.indexWhere((prod) => prod.id == id);
    if (prodIndex >= 0) {
      final url =
          'https://ventaautos-fac9c.firebaseio.com/products/$id.json?auth=$authToken';
      await http.patch(url,
          body: json.encode({
            'title': newProduct.title,
            'price': newProduct.price,
            'description': newProduct.description,
            'km': newProduct.km,
            'phone': newProduct.phone,
            'whatsapp': newProduct.whatsapp,
            'imageUrl01': newProduct.imageUrl01,
            'imageUrl02': newProduct.imageUrl02,
            'imageUrl03': newProduct.imageUrl03,
            'isFavorite': newProduct.isFavorite,
          }));
      _items[prodIndex] = newProduct;
      notifyListeners();
    } else {
      print('...');
    }
  }

  Future<void> deleteProduts(String id) async {
    final url =
        'https://ventaautos-fac9c.firebaseio.com/products/$id.json?auth=$authToken';
    final existingProductIndex = _items.indexWhere((prod) => prod.id == id);
    var existingProduct = _items[existingProductIndex];
    _items.removeAt(existingProductIndex);
    notifyListeners();
    final response = await http.delete(url);
    if (response.statusCode >= 400) {
      _items.insert(existingProductIndex, existingProduct);
      notifyListeners();
      throw HttpException('No se borro el prodcuto');
    }
    existingProduct = null;
  }
}
